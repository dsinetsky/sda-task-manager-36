package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public interface ITaskService extends IAbstractUserOwnedService<Task> {

    @NotNull
    Task create(@Nullable String userId, @Nullable String name) throws GeneralException;

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String desc) throws GeneralException;

    @NotNull
    List<Task> returnTasksOfProject(@Nullable String userId, @Nullable String projectId) throws GeneralException;

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String desc) throws GeneralException;

    @NotNull
    Task updateByIndex(@Nullable String userId, int index, @Nullable String name, @Nullable String desc) throws GeneralException;

    @NotNull
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws GeneralException;

    @NotNull
    Task changeStatusByIndex(@Nullable String userId, int index, @Nullable Status status) throws GeneralException;

}
