package ru.t1.dsinetsky.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private final String taskName = "New Task";

    @NotNull
    private final String taskDescription = "New Task's description";

    @NotNull
    private final List<String> tasksListName = new ArrayList<>(Arrays.asList("TestTask1", "TestTask3", "TestTask2"));

    @NotNull
    private final List<Task> expectedTaskList = new ArrayList<>();

    @NotNull
    private final Sort newSort = Sort.BY_NAME;

    {
        for (@NotNull final String name : tasksListName) {
            @NotNull Task task = new Task(name);
            task.setProjectId(projectId);
            expectedTaskList.add(task);
        }
    }

    @After
    public void after() throws GeneralException {
        taskRepository.clear();
    }

    @Test
    @SneakyThrows
    public void createWithDescriptionTest() {
        final int beforeSize = taskRepository.getSize(userId);
        @NotNull final Task newTask = taskRepository.create(userId, taskName, taskDescription);
        final int difference = taskRepository.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(taskName, newTask.getName());
        Assert.assertEquals(taskDescription, newTask.getDesc());
    }

    @Test
    @SneakyThrows
    public void createWithoutDescriptionTest() {
        final int beforeSize = taskRepository.getSize(userId);
        @NotNull final Task newTask = taskRepository.create(userId, taskName);
        final int difference = taskRepository.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(userId, newTask.getUserId());
        Assert.assertEquals(taskName, newTask.getName());
        Assert.assertTrue(newTask.getDesc().isEmpty());
    }

    @Test
    @SneakyThrows
    public void returnAllTest() {
        taskRepository.clear(userId);
        for (@NotNull final String name : tasksListName) {
            taskRepository.create(userId, name);
        }
        @NotNull final List<Task> currentTaskList = taskRepository.returnAll(userId);
        Assert.assertArrayEquals(currentTaskList.toArray(), expectedTaskList.toArray());
    }

    @Test
    @SneakyThrows
    public void returnAllWithSortTest() {
        taskRepository.clear(userId);
        for (@NotNull final String name : tasksListName) {
            taskRepository.create(userId, name);
        }
        @NotNull final List<Task> currentTaskList = taskRepository.returnAll(userId, newSort.getComparator());
        expectedTaskList.sort(newSort.getComparator());
        Assert.assertArrayEquals(currentTaskList.toArray(), expectedTaskList.toArray());
    }

    @Test
    @SneakyThrows
    public void addTest() {
        @NotNull final Task newTask = new Task(taskName);
        final int beforeSize = taskRepository.getSize(userId);
        @NotNull final Task addedTask = taskRepository.add(userId, newTask);
        final int difference = taskRepository.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(addedTask.getId(), newTask.getId());
        Assert.assertEquals(addedTask, newTask);
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        taskRepository.clear(userId);
        Assert.assertTrue(taskRepository.returnAll(userId).isEmpty());
    }

    @Test
    @SneakyThrows
    public void findByIdTest() {
        @NotNull final Task newTask = new Task(taskName);
        taskRepository.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        final int beforeSize = taskRepository.getSize(userId);
        @Nullable final Task foundTask = taskRepository.findById(userId, taskId);
        Assert.assertEquals(beforeSize, taskRepository.getSize(userId));
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(foundTask.getId(), taskId);
        Assert.assertEquals(foundTask, newTask);
    }

    @Test
    @SneakyThrows
    public void findByIndexTest() {
        @NotNull final Task newTask = new Task(taskName);
        taskRepository.add(userId, newTask);
        final int beforeSize = taskRepository.getSize(userId);
        @Nullable final Task foundTask = taskRepository.findByIndex(userId, beforeSize - 1);
        Assert.assertEquals(beforeSize, taskRepository.getSize(userId));
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(foundTask.getId(), newTask.getId());
        Assert.assertEquals(foundTask, newTask);
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        @NotNull final Task newTask = new Task(taskName);
        taskRepository.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        final int beforeSize = taskRepository.getSize(userId);
        taskRepository.removeById(userId, taskId);
        final int difference = beforeSize - taskRepository.getSize(userId);
        Assert.assertEquals(1, difference);
        Assert.assertFalse(taskRepository.existsById(userId, newTask.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByIndexTest() {
        @NotNull final Task newTask = new Task(taskName);
        taskRepository.add(userId, newTask);
        final int beforeSize = taskRepository.getSize(userId);
        taskRepository.removeByIndex(userId, beforeSize - 1);
        final int difference = beforeSize - taskRepository.getSize(userId);
        Assert.assertEquals(1, difference);
        Assert.assertFalse(taskRepository.existsById(userId, newTask.getId()));
    }

    @Test
    @SneakyThrows
    public void getSizeTest() {
        taskRepository.clear(userId);
        for (@NotNull final String name : tasksListName) {
            taskRepository.create(userId, name);
        }
        Assert.assertEquals(taskRepository.getSize(userId), expectedTaskList.size());
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        @NotNull final Task newTask = new Task(taskName);
        taskRepository.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        Assert.assertTrue(taskRepository.existsById(userId, taskId));
    }

    @Test
    @SneakyThrows
    public void findTasksByProjectId() {
        taskRepository.clear(userId);
        for (@NotNull final String name : tasksListName) {
            @NotNull Task task = new Task(name);
            task.setProjectId(projectId);
            taskRepository.add(userId, task);
        }
        @NotNull final List<Task> currentTaskList = taskRepository.findTasksByProjectId(userId, projectId);
        Assert.assertArrayEquals(currentTaskList.toArray(), expectedTaskList.toArray());
    }

}
