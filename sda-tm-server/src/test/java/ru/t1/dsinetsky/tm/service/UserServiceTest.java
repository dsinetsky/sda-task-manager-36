package ru.t1.dsinetsky.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.field.EntityIdIsEmptyException;
import ru.t1.dsinetsky.tm.exception.user.*;
import ru.t1.dsinetsky.tm.marker.UnitCategory;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.repository.ProjectRepository;
import ru.t1.dsinetsky.tm.repository.TaskRepository;
import ru.t1.dsinetsky.tm.repository.UserRepository;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @NotNull
    private final String userLogin = "Test";

    @NotNull
    private final String userPassword = "Test";

    @NotNull
    private final String userFirstName = "TestName";

    @NotNull
    private final String userLastName = "TestLastName";

    @NotNull
    private final String userMiddleName = "TestMiddleName";

    @NotNull
    private final String userEmail = "TestEmail";

    @NotNull
    private final String taskName = "New Task";

    @NotNull
    private final String projectName = "New Project";

    @NotNull
    private final String taskDescription = "New Task's description";

    @NotNull
    private final String projectDescription = "New Project's description";

    @After
    public void after() throws GeneralException {
        userService.clear();
    }

    @Test
    @SneakyThrows
    public void addPositiveTest() {
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        final int beforeSize = userService.getSize();
        @NotNull final User addedUser = userService.add(newUser);
        Assert.assertThrows(InvalidUserException.class, () -> userService.add((User) null));
        final int difference = userService.getSize() - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(addedUser.getId(), newUser.getId());
        Assert.assertEquals(addedUser, newUser);
    }

    @Test
    @SneakyThrows
    public void addWithoutLoginTest() {
        @NotNull final User newUser = UserBuilder.create().login("").password(userPassword).toUser();
        Assert.assertThrows(IncorrectLoginPasswordException.class, () -> userService.add(newUser));
    }

    @Test
    @SneakyThrows
    public void addWithoutPasswordTest() {
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password("").toUser();
        Assert.assertThrows(IncorrectLoginPasswordException.class, () -> userService.add(newUser));
    }

    @Test
    @SneakyThrows
    public void addDuplicateTest() {
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        @NotNull final User newUser2 = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        Assert.assertThrows(UserAlreadyExistException.class, () -> userService.add(newUser2));
    }

    @Test
    @SneakyThrows
    public void removeUserByLoginTest() {
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.removeUserByLogin(null));
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.removeUserByLogin(""));
        @NotNull final Project newProject = new Project(projectName, projectDescription);
        @NotNull final Task newTask = new Task(taskName, taskDescription);
        @NotNull final User user = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        @NotNull final String userId = user.getId();
        userService.add(user);
        newProject.setUserId(userId);
        newTask.setUserId(userId);
        projectRepository.add(newProject);
        taskRepository.add(newTask);
        final int beforeSize = userService.getSize();
        userService.removeUserByLogin(userLogin);
        final int difference = beforeSize - userService.getSize();
        Assert.assertEquals(1, difference);
        Assert.assertFalse(userService.isUserExistByLogin(userLogin));
        Assert.assertFalse(projectRepository.existsById(userId, newProject.getId()));
        Assert.assertFalse(taskRepository.existsById(userId, newTask.getId()));
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        @NotNull final User user = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(user);
        userService.clear();
        Assert.assertEquals(0, userService.returnAll().size());
    }

    @Test
    @SneakyThrows
    public void findByIdTest() {
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> userService.findById(null));
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> userService.findById(""));
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        @NotNull final String userId = newUser.getId();
        final int beforeSize = userService.getSize();
        @NotNull final User foundUser = userService.findById(userId);
        Assert.assertEquals(beforeSize, userService.getSize());
        Assert.assertEquals(foundUser.getId(), userId);
        Assert.assertEquals(foundUser, newUser);
    }

    @Test
    @SneakyThrows
    public void findUserByLoginTest() {
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.findUserByLogin(null));
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.findUserByLogin(""));
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        final int beforeSize = userService.getSize();
        @NotNull final User foundUser = userService.findUserByLogin(userLogin);
        Assert.assertEquals(beforeSize, userService.getSize());
        Assert.assertEquals(foundUser.getId(), newUser.getId());
        Assert.assertEquals(foundUser, newUser);
    }

    @Test
    @SneakyThrows
    public void updateUserByIdTest() {
        Assert.assertThrows(UserIdIsEmptyException.class, () -> userService.updateUserById(null, userFirstName, userLastName, userMiddleName));
        Assert.assertThrows(UserIdIsEmptyException.class, () -> userService.updateUserById("", userFirstName, userLastName, userMiddleName));
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        final int beforeSize = userService.getSize();
        @NotNull final User updatedUser = userService.updateUserById(newUser.getId(), userFirstName, userLastName, userMiddleName);
        Assert.assertEquals(beforeSize, userService.getSize());
        Assert.assertEquals(userFirstName, updatedUser.getFirstName());
        Assert.assertEquals(userLastName, updatedUser.getLastName());
        Assert.assertEquals(userMiddleName, updatedUser.getMiddleName());
    }

    @Test
    @SneakyThrows
    public void updateUserByLoginTest() {
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.updateUserByLogin(null, userFirstName, userLastName, userMiddleName));
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.updateUserByLogin("", userFirstName, userLastName, userMiddleName));
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        final int beforeSize = userService.getSize();
        @NotNull final User updatedUser = userService.updateUserByLogin(newUser.getLogin(), userFirstName, userLastName, userMiddleName);
        Assert.assertEquals(beforeSize, userService.getSize());
        Assert.assertEquals(userFirstName, updatedUser.getFirstName());
        Assert.assertEquals(userLastName, updatedUser.getLastName());
        Assert.assertEquals(userMiddleName, updatedUser.getMiddleName());
    }

    @Test
    @SneakyThrows
    public void updateEmailByIdTest() {
        Assert.assertThrows(UserIdIsEmptyException.class, () -> userService.updateEmailById(null, userEmail));
        Assert.assertThrows(UserIdIsEmptyException.class, () -> userService.updateEmailById("", userEmail));
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        final int beforeSize = userService.getSize();
        @NotNull final User updatedUser = userService.updateEmailById(newUser.getId(), userEmail);
        Assert.assertEquals(beforeSize, userService.getSize());
        Assert.assertEquals(userEmail, updatedUser.getEmail());
    }

    @Test
    @SneakyThrows
    public void updateEmailByLoginTest() {
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.updateEmailByLogin(null, userEmail));
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.updateEmailByLogin("", userEmail));
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        final int beforeSize = userService.getSize();
        @NotNull final User updatedUser = userService.updateEmailByLogin(newUser.getLogin(), userEmail);
        Assert.assertEquals(beforeSize, userService.getSize());
        Assert.assertEquals(userEmail, updatedUser.getEmail());
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> userService.existsById(null));
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> userService.existsById(""));
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        Assert.assertTrue(userService.existsById(newUser.getId()));
    }

    @Test
    @SneakyThrows
    public void isUserExistByLoginTest() {
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.isUserExistByLogin(null));
        Assert.assertThrows(UserLoginIsEmptyException.class, () -> userService.isUserExistByLogin(""));
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        Assert.assertTrue(userService.isUserExistByLogin(newUser.getLogin()));
    }

    @Test
    @SneakyThrows
    public void getSizeTest() {
        userService.clear();
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userService.add(newUser);
        Assert.assertEquals(1, userService.getSize());
    }

}
