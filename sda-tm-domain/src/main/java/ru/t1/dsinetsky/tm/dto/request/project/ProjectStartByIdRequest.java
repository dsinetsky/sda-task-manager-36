package ru.t1.dsinetsky.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectStartByIdRequest(final @Nullable String token) {
        super(token);
    }

    public ProjectStartByIdRequest(final @Nullable String token, @Nullable final String id) {
        super(token);
        this.id = id;
    }

}
