package ru.t1.dsinetsky.tm.dto.request.user.admin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserLockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLockRequest(final @Nullable String token) {
        super(token);
    }

    public UserLockRequest(final @Nullable String token, @Nullable final String login) {
        super(token);
        this.login = login;
    }

}
