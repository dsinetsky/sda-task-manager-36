package ru.t1.dsinetsky.tm.exception.field;

public final class NegativeIndexException extends GeneralFieldException {

    public NegativeIndexException() {
        super("Index must be greater than zero!");
    }

}
