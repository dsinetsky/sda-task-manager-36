package ru.t1.dsinetsky.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateEmailRequest extends AbstractUserRequest {

    @Nullable
    private String email;

    public UserUpdateEmailRequest(final @Nullable String token) {
        super(token);
    }

    public UserUpdateEmailRequest(final @Nullable String token, @Nullable final String email) {
        super(token);
        this.email = email;
    }

}
