package ru.t1.dsinetsky.tm.dto.response.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.response.AbstractResponse;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<Project> projects;

}
