package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IAdminEndpoint getAdminEndpoint();

    @NotNull
    ITokenService getTokenService();

}
