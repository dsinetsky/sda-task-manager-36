package ru.t1.dsinetsky.tm.service;

import lombok.Getter;
import lombok.Setter;
import ru.t1.dsinetsky.tm.api.service.ITokenService;

@Getter
@Setter
public final class TokenService implements ITokenService {

    private String token;

}
